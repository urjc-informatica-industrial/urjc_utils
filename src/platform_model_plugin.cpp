#include <ros/ros.h>
#include "std_msgs/String.h"
#include "std_msgs/Float32.h"

#include <gazebo/physics/physics.hh>
#include <gazebo/transport/TransportTypes.hh>
#include <gazebo/common/Time.hh>
#include <gazebo/common/Plugin.hh>
#include <gazebo/rendering/Visual.hh>
#include <gazebo/common/Events.hh>

namespace gazebo
{
    class PlatformModelPlugin : public ModelPlugin
    {
        public: void Load(physics::ModelPtr _parent, sdf::ElementPtr _sdf)
        {            
            // Store the pointer to the model
            this->model = _parent;
            this->world = this->model->GetWorld();
            this->sdf = _sdf;
            auto name = this->model->GetName();
            
            if (!ros::isInitialized()) 
            {
                int argc = 0;
                char **argv = NULL;
                ros::init(argc, argv, name);
            }

            this->rosnode_ = new ros::NodeHandle(name);
            auto topic = "/" + name + "/distance";
            this->pub_ = this->rosnode_->advertise<std_msgs::Float32>(topic, 100);

            // Listen to the update event. This event is broadcast every
            // simulation iteration.
            this->updateConnection = event::Events::ConnectWorldUpdateBegin(
                std::bind(&PlatformModelPlugin::OnUpdate, this));
            
        }

        // Called by the world update start event
        public: void OnUpdate()
        {
            auto turtlebot3 = this->world->EntityByName("turtlebot3_burger");

            if (turtlebot3) {
                auto turtlepose = turtlebot3->WorldPose();                
                auto myPose = this->model->WorldPose();
                auto diff = turtlepose.Pos() - myPose.Pos();
                auto distance = myPose.Pos().Distance(turtlepose.Pos());

                std_msgs::Float32 msg;
                msg.data = distance;
                this->pub_.publish(msg);    
            }
        }

        private: ros::NodeHandle* rosnode_;
        private: ros::Publisher pub_;

        // Pointer to the model
        private: physics::ModelPtr model;

        // Pointer to the model
        private: physics::WorldPtr world;

        private: sdf::ElementPtr sdf;

        // Pointer to the update event connection
        private: event::ConnectionPtr updateConnection;

    };

    GZ_REGISTER_MODEL_PLUGIN(PlatformModelPlugin);
}