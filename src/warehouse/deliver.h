#ifndef URJC_UTILS_DELIVER_H
#define URJC_UTILS_DELIVER_H

#include "urjc_utils/DeliverSrv.h"

bool deliver(urjc_utils::DeliverSrv::Request &req, urjc_utils::DeliverSrv::Response &res);

#endif