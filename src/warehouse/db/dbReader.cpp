#include "dbReader.h"

#include <iostream>
#include <stdio.h>
#include <sqlite3.h>
#include <cstdlib>

using namespace urjc_utils;

DBReader::DBReader() {
    int rc = 1;
    const char* env_db = std::getenv("WAREHOUSE_DB");
    std::cout << "Connecting to database: " << env_db << std::endl;

    if (!env_db) {
        std::cerr << "No database defined. Make sure to define WAREHOUSE_DB" << env_db << std::endl;
    } else {
        std::cout << "Open " << env_db << std::endl;
    }
    rc = sqlite3_open(env_db, &db);
    if (rc != SQLITE_OK) {
        fprintf(stderr, "Error al abrir la base de datos: %s.\n", sqlite3_errmsg(db));
    }
}

void DBReader::close() {
    sqlite3_close(db);
}

std::vector<urjc_utils::order_t> DBReader::getOrders() {
    sqlite3_stmt *result;
    int rc = 1;
    std::vector<urjc_utils::order_t> orders;
    
    rc = sqlite3_prepare(db, "SELECT orderId FROM 'Order'", -1, &result, NULL);
    
    if (rc != SQLITE_OK) {
        fprintf(stderr, "(Order) Error in the query: %s.\n", sqlite3_errmsg(db));
    }

    while ( sqlite3_step(result) == SQLITE_ROW) {
        urjc_utils::order_t order;        
        order.orderId = sqlite3_column_int(result, 0);
        order.lines = getOrderLinesInOrder(order.orderId); 

        orders.push_back(order);
    }

    return orders;
}

std::vector<urjc_utils::order_line_t> DBReader::getOrderLinesInOrder(int orderId) {
    sqlite3_stmt *result;
    int rc = 1;
    std::vector<urjc_utils::order_line_t> orderLines;
    
    std::string sql = "";
    sql += "SELECT p.productId, p.name, ol.units ";
    sql += "FROM OrderLine ol JOIN Product p on ol.productId = p.productId ";
    sql += "WHERE orderId = " + std::to_string(orderId);

    rc = sqlite3_prepare(db, sql.c_str(), -1, &result, NULL);
    
    if (rc != SQLITE_OK) {
        fprintf(stderr, "(OrderLines) Error in the query: %s.\n", sqlite3_errmsg(db));
    }

    while ( sqlite3_step(result) == SQLITE_ROW) {
        urjc_utils::order_line_t orderLine;
        orderLine.product.productId = sqlite3_column_int(result, 0);
        auto productName = sqlite3_column_text(result, 1);
        orderLine.product.name += std::string(reinterpret_cast<const char*>(productName));
        orderLine.units = sqlite3_column_int(result, 2);        
        orderLines.push_back(orderLine);
    }

    return orderLines;
}

std::vector<urjc_utils::pickup_zone_t> DBReader::getPickupZones() {
    sqlite3_stmt *result;
    int rc = 1;
    std::vector<urjc_utils::pickup_zone_t> pickupZones;
    
    rc = sqlite3_prepare(db, "SELECT pz.pickupZoneId, pz.name, pz.xLocation, pz.yLocation FROM PickupZone as pz ", -1, &result, NULL);
    
    if (rc != SQLITE_OK) {
        fprintf(stderr, "(PickupZones) Error in the query: %s.\n", sqlite3_errmsg(db));
    }

    while ( sqlite3_step(result) == SQLITE_ROW) {
        urjc_utils::pickup_zone_t pickupZone;        
        pickupZone.pickupZoneId = sqlite3_column_int(result, 0);
        auto pickupZoneName = sqlite3_column_text(result, 1);
        pickupZone.name = std::string(reinterpret_cast<const char*>(pickupZoneName));
        pickupZone.x = (float)sqlite3_column_double(result, 2);
        pickupZone.y = (float)sqlite3_column_double(result, 3);
        pickupZone.inventory = getInventoryInZone(pickupZone.pickupZoneId);

        pickupZones.push_back(pickupZone);
    }

    return pickupZones;
}

std::vector<urjc_utils::inventory_line_t> DBReader::getInventoryInZone(int pickupZoneId) {
    sqlite3_stmt *result;
    int rc = 1;
    std::vector<urjc_utils::inventory_line_t> inventoryLines;
    
    std::string sql = "";
    sql += "SELECT p.productId, p.name, i.units ";
    sql += "FROM Inventory as i JOIN Product p on i.productId = p.productId ";
    sql += "WHERE i.pickupZoneId = " + std::to_string(pickupZoneId);    

    rc = sqlite3_prepare(db, sql.c_str(), -1, &result, NULL);
    
    if (rc != SQLITE_OK) {
        fprintf(stderr, "(Inventory) Error in the query: %s.\n", sqlite3_errmsg(db));
    }

    while ( sqlite3_step(result) == SQLITE_ROW) {
        urjc_utils::inventory_line_t inventoryLine;
        inventoryLine.product.productId = sqlite3_column_int(result, 0);
        auto productName = sqlite3_column_text(result, 1);
        inventoryLine.product.name += std::string(reinterpret_cast<const char*>(productName));
        inventoryLine.units = sqlite3_column_int(result, 2);        
        inventoryLines.push_back(inventoryLine);
    }

    return inventoryLines;
}


void DBReader::testDBReader() {    
    auto orders = getOrders();

    for (int i=0; i < orders.size(); i++) {
        auto order = orders.at(i);
        std::cout << "Order: " << order.orderId << std::endl;

        for (int j=0; j < order.lines.size(); j++) {
            auto orderLine = order.lines.at(j);
            std::cout << "Product: " << orderLine.product.name << " - " << orderLine.units << " units" << std::endl;
        }
    }

    auto pickupZones = getPickupZones();

    for (int i=0; i < pickupZones.size(); i++) {
        pickup_zone_t pickupZone = pickupZones.at(i);
        std::cout << "PickupZone: " << pickupZone.pickupZoneId << "[" << pickupZone.name << "]" 
            << "(" << pickupZone.x << "," << pickupZone.y << ")" << std::endl;

        for (int j=0; j < pickupZone.inventory.size(); j++) {
            auto inventoryLine = pickupZone.inventory.at(j);
            std::cout << "Product: " << inventoryLine.product.name << " - " << inventoryLine.units << " units" << std::endl;
        }
    }
}