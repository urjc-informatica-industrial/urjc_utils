#ifndef URJC_UTILS_DB_READER_H
#define URJC_UTILS_DB_READER_H

#include <iostream>
#include <stdio.h>
#include <sqlite3.h>
#include <cstdlib>
#include <vector>

namespace urjc_utils
{
    struct product_t {
        int productId;
        std::string name;
    };

    struct order_line_t {
        product_t product;
        int units;
    };

    struct inventory_line_t {
        product_t product;
        int units;
    };

    struct order_t {
        int orderId;
        std::vector<order_line_t> lines;
    };

    struct pickup_zone_t {
        int pickupZoneId;
        std::string name;
        float x;
        float y;
        std::vector<inventory_line_t> inventory;
    };

    class DBReader {
        private:
            sqlite3 *db;
            void close();

        public:
            DBReader();
            std::vector<urjc_utils::order_t> getOrders();
            std::vector<urjc_utils::order_line_t> getOrderLinesInOrder(int orderId);
            std::vector<urjc_utils::inventory_line_t> getInventoryInZone(int pickupZoneId);
            urjc_utils::product_t getProduct(int productId);
            std::vector<urjc_utils::pickup_zone_t> getPickupZones();
            void testDBReader();
    };

}

#endif