#ifndef URJC_UTILS_WAREHOUSE_CONTROLLER_H
#define URJC_UTILS_WAREHOUSE_CONTROLLER_H

#include "ros/ros.h"
#include <iostream>
#include <stdio.h>
#include <sqlite3.h>
#include <cstdlib>
#include <vector>
#include "std_msgs/Float32.h"
#include "db/dbReader.h"
#include "urjc_utils/OrderLine.h"

namespace urjc_utils
{


    class WarehouseController {
        private:
            DBReader dbReader;
            ros::NodeHandle* n;
            ros::Publisher pub_;
            WarehouseController();            
            static WarehouseController* instance;
            std::vector<urjc_utils::pickup_zone_t> zones;
            std::vector<urjc_utils::order_t> orders;
            std::vector<urjc_utils::order_line_t> shopping_cart;
            void initializeFromDB();
            urjc_utils::pickup_zone_t* getZone(int zoneId);
            void publishShoppingCart();
            void addLineToShoppingCart(urjc_utils::order_line_t order_line);
            urjc_utils::order_t* getOrderById(int orderId);
            bool lineInCart(int productId, int units);

        public:
            static WarehouseController* getInstance();
            void testDBConnection();
            void setRosNode(ros::NodeHandle* n);
            bool addOrderLineFromZone(int productId, int units, int zoneId);
            std::vector<urjc_utils::pickup_zone_t> getZones();
            /**
             * Returns zone in which is located turtlebot3
             */
            urjc_utils::pickup_zone_t getInAreaZone();
            bool isOrderFulFilled(int orderId);
            bool isTurtlebotInDeliveryArea();
            bool areProductsInShoppingCart(std::vector<urjc_utils::OrderLine> lines);
            void emptyShoppingCart();
    };

}


#endif