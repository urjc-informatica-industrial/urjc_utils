#ifndef URJC_UTILS_PICKUP_H
#define URJC_UTILS_PICKUP_H

#include "urjc_utils/PickUpSrv.h"

bool pickup(urjc_utils::PickUpSrv::Request &req, urjc_utils::PickUpSrv::Response &res);

#endif