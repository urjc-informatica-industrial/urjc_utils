#include "warehouse_controller.h"

#include "ros/ros.h"
#include <iostream>
#include <stdio.h>
#include <cstdlib>
#include "std_msgs/Float32.h"
#include "gazebo_msgs/GetModelState.h"
#include "urjc_utils/ShoppingCart.h"
#include "urjc_utils/OrderLine.h"

using namespace urjc_utils;

WarehouseController* WarehouseController::instance = 0;

WarehouseController::WarehouseController() {
    std::cout << "Initialize controller. " << std::endl;
    initializeFromDB();
    std::cout << "DB Initialized. " << std::endl;
    
}

WarehouseController* WarehouseController::getInstance() {
    if(!instance) {
        instance = new WarehouseController();
    }
    return instance;
}

void WarehouseController::setRosNode(ros::NodeHandle* node) {
    n = node;
    auto topic = "/shopping_cart";
    this->pub_ = this->n->advertise<ShoppingCart>(topic, 10);
    publishShoppingCart();
}

void WarehouseController::initializeFromDB() {
    orders = dbReader.getOrders();

    for (int i=0; i < orders.size(); i++) {
        auto order = orders.at(i);
        ROS_INFO("Order: %d", order.orderId);

        for (int j=0; j < order.lines.size(); j++) {
            auto orderLine = order.lines.at(j);
            ROS_INFO("Product: %s - %d units", orderLine.product.name.c_str(), orderLine.units);
        }
    }

    zones = dbReader.getPickupZones();

    for (int i=0; i < zones.size(); i++) {
        pickup_zone_t pickupZone = zones.at(i);
        
        ROS_INFO("PickupZone: %d [%s] ( %.4f,%.4f)", pickupZone.pickupZoneId, pickupZone.name.c_str(), pickupZone.x, pickupZone.y);

        for (int j=0; j < pickupZone.inventory.size(); j++) {
            auto inventoryLine = pickupZone.inventory.at(j);
            ROS_INFO("Product: %s - %d units", inventoryLine.product.name.c_str(), inventoryLine.units);
        }
    }
}

bool WarehouseController::addOrderLineFromZone(int productId, int units, int zoneId) {    
    auto zone = getZone(zoneId);

    if (zone && zone->pickupZoneId != -1) {
        // Check inventory has enought products
        for (auto &inventoryLine : zone->inventory) {
            // auto inventoryLine = zone.inventory.at(i);
            if (productId == inventoryLine.product.productId) {                
                ROS_INFO("Product: %s is at %s zone", inventoryLine.product.name.c_str(), zone->name.c_str());

                if (units <= inventoryLine.units) {
                    ROS_INFO("There are enough units: %d of available inventory %d (remain %d)", units, inventoryLine.units, (inventoryLine.units - units));
                    
                    inventoryLine.units = inventoryLine.units - units;

                    urjc_utils::order_line_t order_line;
                    order_line.product = inventoryLine.product;
                    order_line.units = units;
                    addLineToShoppingCart(order_line);

                    return true;
                } else {
                    ROS_WARN("There are NOT enough units: %d of available inventory %d", units, inventoryLine.units);
                }

            }
        }
        ROS_WARN("Not products at the pickupzone");
    }

    return false;
}

void WarehouseController::addLineToShoppingCart(urjc_utils::order_line_t order_line) {
    bool added = false;
    for( auto &line : shopping_cart ) {
        if (line.product.productId == order_line.product.productId) {
            line.units += order_line.units;
            added = true;
            break;
        }
    }

    if (!added) {
        shopping_cart.push_back(order_line);
    }

    publishShoppingCart();
}

void WarehouseController::publishShoppingCart() {
    urjc_utils::ShoppingCart msg;

    for (int i=0; i<shopping_cart.size(); i++) {
        auto line = shopping_cart.at(i);
        urjc_utils::OrderLine order_line;
        order_line.productId = line.product.productId;
        order_line.units = line.units;
        msg.lines.push_back(order_line);
    }
    
    this->pub_.publish(msg); 
}

urjc_utils::pickup_zone_t* WarehouseController::getZone(int zoneId) {
    
    for (int i=0; i < zones.size(); i++) {
        auto zone = zones.at(i);

        if (zone.pickupZoneId == zoneId) {
            return &zones.at(i);
        }
    }

    return nullptr;
}

urjc_utils::pickup_zone_t WarehouseController::getInAreaZone() {
    urjc_utils::pickup_zone_t empty;
    empty.pickupZoneId = -1;

    auto client = n->serviceClient<gazebo_msgs::GetModelState>("/gazebo/get_model_state");
    
    gazebo_msgs::GetModelState getModelState;
    getModelState.request.model_name = "turtlebot3_burger";

    if(client.call(getModelState))
    {
        float x = getModelState.response.pose.position.x;
        float y = getModelState.response.pose.position.y;

        for (int i=0; i < zones.size(); i++) {
            urjc_utils::pickup_zone_t zone = zones.at(i);

            auto distance = (zone.x - x)*(zone.x - x) + (zone.y - y)*(zone.y - y);

            if (distance <= (0.5 * 0.5)) {
                ROS_INFO("Turtlebot in zone %s", zone.name.c_str());
                return zone;
            }
        }

        ROS_WARN("Turtlebot is NOT in ANY pickup area");
    }
    else {
        ROS_ERROR("Failed to call service");
    }

    return empty;
}

bool WarehouseController::isTurtlebotInDeliveryArea() {
    auto client = n->serviceClient<gazebo_msgs::GetModelState>("/gazebo/get_model_state");
    
    gazebo_msgs::GetModelState getModelState;
    getModelState.request.model_name = "turtlebot3_burger";

    if(client.call(getModelState))
    {
        float x = getModelState.response.pose.position.x;
        float y = getModelState.response.pose.position.y;

        float deliverX = 0;
        float deliverY = 0;

        auto distance = (deliverX - x)*(deliverX - x) + (deliverY - y)*(deliverY - y);

        if (distance <= 1) {
            ROS_INFO("Turtlebot is in delivery zone");
            return true;
        } else {
            ROS_WARN("Turtlebot is NOT in delivery zone");
        }        
    }
    else {
        ROS_ERROR("Failed to call service");
    }

    return false;
}

bool WarehouseController::areProductsInShoppingCart(std::vector<urjc_utils::OrderLine> lines) {
    auto allInCart = true;

    for (auto line : lines)
    {
        bool inCart = lineInCart(line.productId, line.units);        
        allInCart = allInCart && inCart;
    }

    return allInCart;
}

bool WarehouseController::lineInCart(int productId, int units)
{
    bool inCart = false;
    for (auto scline : shopping_cart)
    {
        if (!inCart && scline.product.productId == productId)
        {
            ROS_DEBUG("Product %d is at cart", productId);
            if (scline.units == units)
            {
                ROS_DEBUG("Product %d has %d units: OK", productId, units);
                inCart = true;
            }
            else
            {
                ROS_WARN("Product %d has %d units in shopping cart but requesting %d units",
                        productId, scline.units, units);
            }
        }
    }

    return inCart;
}

bool WarehouseController::isOrderFulFilled(int orderId)
{
    bool orderFulFilled = true;

    auto order = getOrderById(orderId);

    if (order) {
        for (auto oline : order->lines) {
            bool inCart = lineInCart(oline.product.productId, oline.units);           
            orderFulFilled = orderFulFilled && inCart;
        }
    } else {
        ROS_ERROR("Order with ID %d DOESN'T exist", orderId);
        return false;
    }

    return orderFulFilled;
}

urjc_utils::order_t* WarehouseController::getOrderById(int orderId) {

    for(auto &order : orders) {
        if (order.orderId == orderId) {
            return &order;
        }
    }

    return nullptr;
}

void WarehouseController::emptyShoppingCart() {
    shopping_cart.clear();
    publishShoppingCart();
}

void WarehouseController::testDBConnection()
{
    dbReader.testDBReader();
}

std::vector<urjc_utils::pickup_zone_t> WarehouseController::getZones() {
    return zones;
}