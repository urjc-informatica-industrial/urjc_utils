#include "urjc_utils/DeliverSrv.h"
#include "urjc_utils/OrderLine.h"
#include "deliver.h"
#include "warehouse_controller.h"

bool deliver(urjc_utils::DeliverSrv::Request  &req, urjc_utils::DeliverSrv::Response &res) {
    res.done = false;

    auto controller = urjc_utils::WarehouseController::getInstance();

    if(controller->isTurtlebotInDeliveryArea()) {
        /*
        int64 orderId
        OrderLine[] lines
        */
       bool productsInShoppingCart = controller->areProductsInShoppingCart(req.lines);
       if (!productsInShoppingCart) 
           ROS_INFO("Missing products in shopping cart");
       bool orderFulfilled = controller->isOrderFulFilled(req.orderId);
       if (!orderFulfilled)
           ROS_INFO("Missing products in order");

       if (productsInShoppingCart && orderFulfilled) {
           res.done = true;
           controller->emptyShoppingCart();
           ROS_INFO("Order %ld has been fulfilled", req.orderId);
       }
    }

    return true;
}