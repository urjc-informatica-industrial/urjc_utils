#include "pickup.h"
#include "urjc_utils/PickUpSrv.h"
#include "warehouse_controller.h"

bool pickup(urjc_utils::PickUpSrv::Request  &req,
         urjc_utils::PickUpSrv::Response &res)
{
  bool validRequest = true;

  auto controller = urjc_utils::WarehouseController::getInstance();

  urjc_utils::pickup_zone_t pickupZone = controller->getInAreaZone();

  if (pickupZone.pickupZoneId >= 0) {
    res.done = controller->addOrderLineFromZone(req.order.productId, req.order.units, pickupZone.pickupZoneId);
  } else {
    res.done = false;
  }
  
  return validRequest;
}