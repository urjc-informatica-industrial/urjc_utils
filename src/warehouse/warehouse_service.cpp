#include "ros/ros.h"
#include "deliver.h"
#include "pickup.h"
#include <boost/bind.hpp>
#include <boost/function.hpp>
#include "warehouse_controller.h"
#include "std_msgs/Float32.h"

int main(int argc, char **argv)
{
    ros::init(argc, argv, "warehouse_service");
    ros::NodeHandle n;

    ros::ServiceServer pickupService = n.advertiseService("pickup", pickup);
    ros::ServiceServer deliverService = n.advertiseService("deliver", deliver);
    ROS_INFO("Ready pick up and delivery service");

    auto controller = urjc_utils::WarehouseController::getInstance();
    controller->setRosNode(&n);
    controller->getInAreaZone();

    ros::spin();    

    return 0;
}