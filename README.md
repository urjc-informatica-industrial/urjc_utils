# URJC Utils

This project is used in "Informática Industrial" at URJC

Steps to install and use:

```sh
cd ~/catkin_ws/src
git clone https://gitlab.com/urjc-informatica-industrial/urjc_utils
cd ..
catkin_make
```

To launch the project execute:

```sh
roslaunch urjc_utils block_world.launch
```
